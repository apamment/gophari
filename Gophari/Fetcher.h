#pragma once
#include <string>
#include <curl/curl.h>



class Fetcher
{
public:
	std::string fetch(std::string host, int port, std::string selector, char type, std::string *fname);
	std::string convert(std::string host, int port, std::string selector, char type, bool darkmode, std::string *fname = nullptr);
	CURLcode res;
private:
	std::string sanatize(std::string data);
	CURL* curl;
};

