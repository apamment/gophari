#pragma once
#include <wx/html/htmlwin.h>
#include <wx/menu.h>

class MyFrame;

class GopherView :
    public wxHtmlWindow
{
public:
    void OnCellHover(wxHtmlCellEvent& event);
    void OnCellClicked(wxHtmlCellEvent& event);
    GopherView(wxWindow* win, wxWindowID id, MyFrame* frame);
    void SaveLinkAs(wxCommandEvent& event);
    DECLARE_EVENT_TABLE()
private:
    MyFrame* frame;
    std::string target;
    wxMenu menu;
    bool ConvertUrl(std::string &host, int &port, std::string &sel, char &type);
};

