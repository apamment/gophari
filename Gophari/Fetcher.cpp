#include <sstream>
#include <vector>
#include <filesystem>

#include "Fetcher.h"

static size_t my_fwrite(void* buffer, size_t size, size_t nmemb, void* stream) {
	size_t buffersize = size * nmemb;
	std::stringstream* ss = (std::stringstream*)stream;

	ss->write((const char*)buffer, buffersize);

	return buffersize;
}

static size_t my_fwrite_b(void* buffer, size_t size, size_t nmemb, void* stream) {
	size_t buffersize = size * nmemb;
	FILE* fptr= (FILE*)stream;

	fwrite(buffer, size, nmemb, fptr);
	return buffersize;
}

std::string Fetcher::fetch(std::string host, int port, std::string selector, char type, std::string *fname) {
	std::stringstream url;
	std::stringstream data;

	url << "gopher://" << host << ":" << port << "/" << type << "/" << selector;
	curl_global_init(CURL_GLOBAL_DEFAULT);

	curl = curl_easy_init();

	curl_easy_setopt(curl, CURLOPT_URL, url.str().c_str());

	if (fname == nullptr) {
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_fwrite);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
		res = curl_easy_perform(curl);
	}
	else {
		FILE* fptr = fopen(fname->c_str(), "wb");
		if (fptr) {
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_fwrite_b);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, fptr);
			res = curl_easy_perform(curl);
			fclose(fptr);
			data << "<h1>File Saved!</h1><p>" << *fname << " saved to disk." << "</p>";
		}
		else {
			data << "<h1>Error</h1><p>Unable to open " << *fname << " for writing!" << "</p>";
		}
	}

	

	curl_easy_cleanup(curl);
	curl_global_cleanup();

	return data.str();
}

std::string Fetcher::sanatize(std::string data) {
	std::stringstream ss;

	for (size_t i = 0; i < data.size(); i++) {
		switch (data.at(i)) {
		case '<':
			ss << "&lt;";
			break;
		case '>':
			ss << "&gt;";
			break;
		case '&':
			ss << "&amp;";
			break;
		default:
			ss << data.at(i);
		}
	}
	return ss.str();
}

std::string Fetcher::convert(std::string host, int port, std::string selector, char type, bool darkmode, std::string* fname) {
	std::stringstream result;

	std::string data = fetch(host, port, selector, type, fname);

	result << "<head><meta charset=\"UTF-8\"></head>";

	if (darkmode) {
		result << "<body bgcolor=\"#222222\" text=\"#dddddd\" link=\"#5961af\">";
	}
	else {
		result << "<body>";
	}
	if (res != CURLE_OK) {
		result << "<h1>Error</h1>\n<p>An Error Occured</p>\n";
	}
	else {
		switch (type) {
		case '1':
		{
			std::string line;
			std::istringstream iss(data);
			result << "<pre>";
			while (std::getline(iss, line)) {
				std::string token;
				std::vector<std::string> tokens;
				std::istringstream iss2(line);
				while (std::getline(iss2, token, '\t')) {
					tokens.push_back(token);
				}

				if (tokens.size() > 0) {
					if (tokens.at(0).size() == 0) continue;
					if (tokens.at(0).at(0) == '.') continue;
					if (tokens.at(0).at(0) == 'i') {
						result << "   " << sanatize(tokens.at(0).substr(1)) << "<br />";
					}
					else {
						std::string host;
						int port;
						std::string selector;

						if (tokens.size() > 1) {
							selector = tokens.at(1);
						}
						else {
							selector = "";
						}

						if (tokens.size() > 2) {
							host = tokens.at(2);
						}
						else {
							host = "";
						}

						if (tokens.size() > 3) {
							port = strtol(tokens.at(3).c_str(), NULL, 10);
						}
						else {
							port = 70;
						}
						if (tokens.at(0).at(0) == '1') {
							result << "<img src=\"memory:folder.png\" />";
						}
						else if (tokens.at(0).at(0) == '0') {
							result << "<img src=\"memory:text.png\" />";
						}
						else if (tokens.at(0).at(0) == '9' || tokens.at(0).at(0) == '5') {
							result << "<img src=\"memory:binary.png\" />";
						}
						else if (tokens.at(0).at(0) == '7') {
							result << "<img src=\"memory:search.png\" />";
						}
						else if (tokens.at(0).at(0) == 'I' || tokens.at(0).at(0) == 'g') {
							result << "<img src=\"memory:image.png\" />";
						}
						else if (tokens.at(0).at(0) == 's') {
							result << "<img src=\"memory:sound.png\" />";
						}
						else {
							result << "<img src=\"memory:unknown.png\" />";
						}
						if (selector.substr(0, 4) == "URL:") {
							result << "<a href=\"" << selector.substr(4) << "\">" << sanatize(tokens.at(0).substr(1)) << "</a><br />";
						}
						else {

							result << "<a href=\"gopher://" << host << ":" << port << "/" << tokens.at(0).at(0);
							
							if (selector.size() > 0  && selector.at(0) != '/') {
								result << '/';
							}
							result << selector << "\">" << sanatize(tokens.at(0).substr(1)) << "</a><br />";
						}
					}
				}
			}
			result << "</pre>";
		}
			break;
		case '0':
			result << "<pre>" << sanatize(data) << "</pre>";
			break;
		default:
			result << data;
			break;
		}
	}
	result << "</body>";
	return result.str();
}