#pragma once

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <vector>
#include "GopherView.h"
#include "Fetcher.h"

enum
{
    ID_GoButton = 1,
    ID_BackButton,
    ID_HostText,
    ID_PortText,
    ID_SelectorText,
    ID_SetHome,
    ID_SetDarkMode,
    ID_SaveLinkAs
};

struct history_t {
    std::string host;
    int port;
    char type;
    std::string selector;
};

class MyFrame :
    public wxFrame
{
public:
    MyFrame();
    wxTextCtrl* host;
    wxTextCtrl* port;
    wxTextCtrl* selector;
    void fetch(std::string host, int port, std::string selector, char type);
    std::vector<struct history_t> history;
    void setStatus(wxString status);
    bool darkmode;
private:
    GopherView* content;

    Fetcher* fetcher;

    void OnShow(wxShowEvent& event);
    void OnGo(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OnBack(wxCommandEvent& event);
    void OnSetHome(wxCommandEvent& event);
    void OnDarkMode(wxCommandEvent& event);

};

