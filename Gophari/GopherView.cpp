#include <sstream>
#include <cstring>
#include "GopherView.h"
#include "MyFrame.h"
#ifdef _MSC_VER
#include <windows.h>
#include <shellapi.h>
#endif

BEGIN_EVENT_TABLE(GopherView, wxHtmlWindow)
EVT_HTML_CELL_HOVER(wxID_ANY, GopherView::OnCellHover)
EVT_HTML_CELL_CLICKED(wxID_ANY, GopherView::OnCellClicked)
END_EVENT_TABLE()

GopherView::GopherView(wxWindow* win, wxWindowID id, MyFrame* frame) : wxHtmlWindow(win, id) {
    this->frame = frame;
    menu.Append(ID_SaveLinkAs, wxT("&Save link as..."));
    Bind(wxEVT_MENU, &GopherView::SaveLinkAs, this, ID_SaveLinkAs);
}

void GopherView::OnCellClicked(wxHtmlCellEvent& event) {
    wxHtmlLinkInfo* link = event.GetCell()->GetLink();
    if (link != NULL) {
        target = link->GetHref().ToStdString();

        if (event.GetMouseEvent().Button(wxMOUSE_BTN_RIGHT)) {
            PopupMenu(&menu, event.GetMouseEvent().GetPosition());
        }
        else {

            frame->setStatus("");

            if (target.find("gopher://") == 0) {
                std::string host;
                int port = 70;
                std::string selector;
                char type;

                if (ConvertUrl(host, port, selector, type)) {
                    frame->host->SetValue(host);
                    frame->port->SetValue(std::to_string(port));
                    frame->selector->SetValue(selector);

                    struct history_t h;
                    h.host = host;
                    h.port = port;
                    h.type = type;
                    h.selector = selector;

                    frame->history.push_back(h);
                    frame->fetch(host, port, selector, type);
                }
            }
            else {
                // launch firefox
#ifdef _MSC_VER
                ShellExecuteA(0, 0, target.c_str(), 0, 0, SW_SHOW);
#else
                pid_t pid = fork();
                if (pid == 0) {
                    char buffer[PATH_MAX];
                    snprintf(buffer, PATH_MAX, "/bin/sh -c \"xdg-open %s\"", target.c_str());
                    system(buffer);
                    exit(-1);
                }
#endif
            }

        }
    }
}

void GopherView::OnCellHover(wxHtmlCellEvent& event){
    wxHtmlLinkInfo *link =  event.GetCell()->GetLink();

    if (link != NULL) {
        frame->setStatus("URL: " + link->GetHref());
    }
    else {
        frame->setStatus("");
    }
}

bool GopherView::ConvertUrl(std::string &host, int &port, std::string &sel, char &type) {
    std::istringstream iss(target.substr(9));
    std::vector<std::string> tokens;
    std::string token;

    while (std::getline(iss, token, '/')) {
        tokens.push_back(token);
    }

    if (tokens.size() > 1) {
        host = tokens.at(0);

        if (host.rfind(':') != std::string::npos) {
            port = strtol(host.substr(host.rfind(':') + 1).c_str(), NULL, 10);
            host = host.substr(0, host.rfind(':'));
        }

        type = tokens.at(1).at(0);
        std::stringstream selector;
        for (size_t z = 2; z < tokens.size(); z++) {
            selector << tokens.at(z);
            if (z < tokens.size() - 1) {
                selector << '/';
            }
        }

        sel = selector.str();
        return true;
    }

    return false;
}
void GopherView::SaveLinkAs(wxCommandEvent& event) {
    if (target.find("gopher://") == 0) {
        std::string host;
        int port = 70;
        std::string selector;
        char type;

        if (ConvertUrl(host, port, selector, type)) {
            frame->host->SetValue(host);
            frame->port->SetValue(std::to_string(port));
            frame->selector->SetValue(selector);

            struct history_t h;
            h.host = host;
            h.port = port;
            h.type = type;
            h.selector = selector;
            frame->history.push_back(h);
            frame->fetch(host, port, selector, 'b');
        }
    }
    else {
        // launch firefox
#ifdef _MSC_VER
        ShellExecuteA(0, 0, target.c_str(), 0, 0, SW_SHOW);
#else
        pid_t pid = fork();
        if (pid == 0) {
            char buffer[PATH_MAX];
            snprintf(buffer, PATH_MAX, "/bin/sh -c \"xdg-open %s\"", target.c_str());
            system(buffer);
            exit(-1);
        }
#endif
    }
}