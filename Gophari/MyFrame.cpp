#ifdef _MSC_VER
#include <Shlobj.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <iconv.h>
#endif
#include <filesystem>
#include "MyFrame.h"

#define VERSION "0.8"

/* back.png - 335 bytes */
static const unsigned char back_png[] = {
  0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a,
  0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
  0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18,
  0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
  0xf8, 0x00, 0x00, 0x00, 0x06, 0x62, 0x4b, 0x47,
  0x44, 0x00, 0x1f, 0x00, 0x64, 0x00, 0x09, 0x2d,
  0xf1, 0x60, 0x74, 0x00, 0x00, 0x00, 0x09, 0x70,
  0x48, 0x59, 0x73, 0x00, 0x00, 0x2e, 0x23, 0x00,
  0x00, 0x2e, 0x23, 0x01, 0x78, 0xa5, 0x3f, 0x76,
  0x00, 0x00, 0x00, 0x07, 0x74, 0x49, 0x4d, 0x45,
  0x07, 0xe5, 0x03, 0x1b, 0x0b, 0x09, 0x0f, 0x07,
  0xcd, 0xc3, 0xe6, 0x00, 0x00, 0x00, 0x19, 0x74,
  0x45, 0x58, 0x74, 0x43, 0x6f, 0x6d, 0x6d, 0x65,
  0x6e, 0x74, 0x00, 0x43, 0x72, 0x65, 0x61, 0x74,
  0x65, 0x64, 0x20, 0x77, 0x69, 0x74, 0x68, 0x20,
  0x47, 0x49, 0x4d, 0x50, 0x57, 0x81, 0x0e, 0x17,
  0x00, 0x00, 0x00, 0xb7, 0x49, 0x44, 0x41, 0x54,
  0x48, 0xc7, 0x63, 0x60, 0x18, 0x44, 0xe0, 0x3f,
  0x39, 0x9a, 0x98, 0x68, 0x69, 0x38, 0xb1, 0x16,
  0xfc, 0xcf, 0xd9, 0x14, 0x46, 0xbb, 0x60, 0xc9,
  0xd9, 0x14, 0xf6, 0x5f, 0x3e, 0x85, 0xf3, 0x3f,
  0x2d, 0x82, 0xe8, 0x7f, 0xce, 0xa6, 0x30, 0x86,
  0xcd, 0x9b, 0x36, 0x53, 0xe4, 0x42, 0x26, 0x5a,
  0x1a, 0xce, 0xc0, 0xc0, 0xc0, 0xc0, 0x42, 0xa2,
  0xe1, 0xa4, 0x04, 0x13, 0x23, 0x9c, 0x20, 0xc6,
  0x70, 0x5f, 0x3f, 0x5f, 0xa2, 0x4d, 0x9e, 0xe2,
  0xb7, 0x0a, 0xab, 0x05, 0x54, 0x0b, 0x96, 0x87,
  0x73, 0xbe, 0xc3, 0xcd, 0x66, 0xa2, 0xb6, 0xe1,
  0xd8, 0x22, 0x99, 0x66, 0x86, 0xc3, 0x7d, 0x40,
  0x2b, 0xc3, 0x61, 0x16, 0x30, 0x42, 0xc3, 0x8c,
  0x76, 0x3e, 0xa0, 0xa5, 0x25, 0x18, 0xc9, 0x54,
  0x3e, 0x85, 0x13, 0x43, 0xd1, 0xe7, 0xfb, 0xbf,
  0x48, 0x32, 0xf4, 0xdd, 0xde, 0xbf, 0x38, 0xf3,
  0x01, 0x56, 0x4b, 0x3e, 0xdf, 0xff, 0x05, 0xd3,
  0x44, 0xb2, 0xe3, 0xb1, 0x15, 0x15, 0xf8, 0x82,
  0x8b, 0x91, 0x04, 0x8c, 0xb7, 0x2c, 0xa2, 0x5a,
  0x9c, 0xe0, 0x2b, 0x4d, 0x69, 0x9a, 0xba, 0x50,
  0xe2, 0x44, 0xc8, 0x99, 0x99, 0xec, 0xfa, 0x80,
  0x94, 0xfa, 0xf8, 0x3f, 0xcd, 0x7d, 0xc2, 0x30,
  0x18, 0x01, 0x00, 0xc7, 0x6b, 0x53, 0x03, 0x57,
  0xd5, 0xb5, 0x4e, 0x00, 0x00, 0x00, 0x00, 0x49,
  0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82 };



MyFrame::MyFrame() :wxFrame(NULL, wxID_ANY, "Gophari - v" VERSION, wxDefaultPosition, wxSize(800, 600)) {
	wxMenu* menuFile = new wxMenu;
	
#ifdef _MSC_VER
	SetIcons(wxIconBundle(L"IDI_ICON1", 0));
#endif
	menuFile->Append(ID_SetHome, "&Set Home..\tCtrl-H", "Set the home gopher page");
	menuFile->AppendSeparator();
	menuFile->Append(ID_SetDarkMode, "Toggle &Dark Mode", "Toggle Dark Mode");
	menuFile->AppendSeparator();
	menuFile->Append(wxID_EXIT);

	wxMenu* menuHelp = new wxMenu;
	menuHelp->Append(wxID_ABOUT);

	wxMenuBar* menuBar = new wxMenuBar;
	menuBar->Append(menuFile, "&File");
	menuBar->Append(menuHelp, "&Help");

	wxPanel* panel = new wxPanel(this);

	wxBoxSizer* vbox = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer* hbox = new wxBoxSizer(wxHORIZONTAL);
	

	wxStaticText* hlbl = new wxStaticText(panel, wxID_ANY, "Host:");
	host = new wxTextCtrl(panel, ID_HostText);
	
	auto backbitmap = wxBitmap::NewFromPNGData(back_png, WXSIZEOF(back_png));

	wxBitmapButton* backbtn = new wxBitmapButton(panel, ID_BackButton, backbitmap);

	hbox->Add(backbtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
	hbox->Add(hlbl, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
	hbox->Add(host, 1, wxALIGN_CENTER_VERTICAL | wxALL, 5);


	wxStaticText* plbl = new wxStaticText(panel, wxID_ANY, "Port:");
	port = new wxTextCtrl(panel, ID_PortText);
	port->SetValue("70");
	hbox->Add(plbl, 0,  wxALIGN_CENTER_VERTICAL | wxALL, 5);
	hbox->Add(port, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

	wxStaticText* slbl = new wxStaticText(panel, wxID_ANY, "Selector:");
	selector = new wxTextCtrl(panel, ID_SelectorText);
	hbox->Add(slbl, 0,  wxALIGN_CENTER_VERTICAL | wxALL, 5);
	hbox->Add(selector, 1, wxALIGN_CENTER_VERTICAL | wxALL, 5);
	
	wxButton* gobtn = new wxButton(panel, ID_GoButton, "GO");
	hbox->Add(gobtn, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);

	vbox->Add(hbox, 0, wxALIGN_LEFT | wxTOP | wxEXPAND);
	

	content = new GopherView(panel, wxID_ANY, this);
	vbox->Add(content, 1, wxEXPAND);

	fetcher = new Fetcher();



	panel->SetSizer(vbox);
	SetMenuBar(menuBar);

	CreateStatusBar();
	SetStatusText("Welcome to Gophari!");

	history.clear();

	Bind(wxEVT_TEXT_ENTER, &MyFrame::OnGo, this, ID_HostText);
	Bind(wxEVT_TEXT_ENTER, &MyFrame::OnGo, this, ID_PortText);
	Bind(wxEVT_TEXT_ENTER, &MyFrame::OnGo, this, ID_SelectorText);
	Bind(wxEVT_BUTTON, &MyFrame::OnBack, this, ID_BackButton);
	Bind(wxEVT_BUTTON, &MyFrame::OnGo, this, ID_GoButton);
	Bind(wxEVT_MENU, &MyFrame::OnSetHome, this, ID_SetHome);
	Bind(wxEVT_MENU, &MyFrame::OnDarkMode, this, ID_SetDarkMode);
	Bind(wxEVT_MENU, &MyFrame::OnAbout, this, wxID_ABOUT);
	Bind(wxEVT_MENU, &MyFrame::OnExit, this, wxID_EXIT);
	Bind(wxEVT_SHOW, &MyFrame::OnShow, this);

	backbtn->MoveAfterInTabOrder(gobtn);
}

void MyFrame::OnExit(wxCommandEvent& event) {
	Close(true);
}

void MyFrame::OnShow(wxShowEvent& event) {
	wxString homehost;
	wxString homeport;
	wxString homeselector;
	wxConfig* config = new wxConfig("Gophari");

	if (config->Read("HomeHost", &homehost)) {
		host->SetValue(homehost);
	}
	if (config->Read("HomePort", &homeport)) {
		port->SetValue(homeport);
	}
	if (config->Read("HomeSelector", &homeselector)) {
		selector->SetValue(homeselector);
	}

	darkmode  = config->ReadBool("DarkMode", false);

	delete config;
	int p;

	if (host->GetValue() != "") {

		p = strtol(port->GetValue().c_str(), NULL, 10);

		struct history_t h;
		h.host = host->GetValue().ToStdString();
		h.port = p;
		h.type = '1';
		h.selector = selector->GetValue().ToStdString();

		history.push_back(h);

		fetch(host->GetValue().ToStdString(), p, selector->GetValue().ToStdString(), '1');

	}
}

void MyFrame::OnAbout(wxCommandEvent& event) {
	wxMessageBox("Gophari - Gopher Client", "About Gophari", wxOK | wxICON_INFORMATION);
}

void MyFrame::OnBack(wxCommandEvent& event)
{
	if (history.size() > 1) {
		history.pop_back();

		struct history_t h = history.back();

		host->SetValue(h.host);
		port->SetValue(std::to_string(h.port));
		selector->SetValue(h.selector);

		fetch(h.host, h.port, h.selector, h.type);
	}
}


void MyFrame::OnGo(wxCommandEvent& event) {
	int p;

	p = strtol(port->GetValue().c_str(), NULL, 10);

	struct history_t h;
	h.host = host->GetValue().ToStdString();
	h.port = p;
	h.type = '1';
	h.selector = selector->GetValue().ToStdString();

	history.push_back(h);

	fetch(host->GetValue().ToStdString(), p, selector->GetValue().ToStdString(), '1');
}

void MyFrame::OnSetHome(wxCommandEvent& event) {
	wxConfig* config = new wxConfig("Gophari");

	config->Write("HomeHost", host->GetValue());
	config->Write("HomePort", port->GetValue());
	config->Write("HomeSelector", selector->GetValue());

	delete config;
}

void MyFrame::OnDarkMode(wxCommandEvent& event) {
	darkmode = !darkmode;
	wxConfig* config = new wxConfig("Gophari");

	config->Write("DarkMode", darkmode);

	delete config;
	if (history.size() > 0) {
		struct history_t h = history.back();

		host->SetValue(h.host);
		port->SetValue(std::to_string(h.port));
		selector->SetValue(h.selector);

		fetch(h.host, h.port, h.selector, h.type);
	}
}

void MyFrame::fetch(std::string host, int port, std::string selector, char type) {
	std::string data;

	if (type == '0' || type == '1' || type == 'h') {
		data = fetcher->convert(host, port, selector, type, darkmode);

		content->Scroll(wxPoint(0, 0));
	}
	else if (type == '7') {
		
		wxTextEntryDialog entry(this, "Enter a Query", "Query", "");
		entry.ShowModal();
		
		std::string query = entry.GetValue().ToStdString();

		if (query != "") {
			data = fetcher->convert(host, port, selector + "%09" + query, '1', darkmode, nullptr);

			content->Scroll(wxPoint(0, 0));
		}
		else {
			if (history.size() > 1) {
				history.pop_back();
				struct history_t h = history.back();

				this->host->SetValue(h.host);
				this->port->SetValue(std::to_string(h.port));
				this->selector->SetValue(h.selector);
				data = fetcher->convert(h.host, h.port, h.selector, h.type, darkmode, nullptr);

			}
		}

	}
	else {

		std::filesystem::path filename;
#ifdef _MSC_VER
		PWSTR str;
		if (SHGetKnownFolderPath(FOLDERID_Downloads, 0, NULL, &str) == S_OK) {
			filename.assign(str);
		}
		else {
			filename.assign(std::filesystem::current_path());
		}
		CoTaskMemFree(str);
#else
		struct passwd* pw = getpwuid(getuid());

		filename.assign(std::string(pw->pw_dir));
#endif
		
		filename.append(std::filesystem::path(selector).filename().u8string());
		std::string fname = wxSaveFileSelector(std::filesystem::path(selector).filename().u8string(), std::filesystem::path(selector).extension().u8string(), filename.u8string()).ToStdString();

		if (fname.size() > 0) {
			data = fetcher->convert(host, port, selector, type, darkmode, &fname);

			content->Scroll(wxPoint(0, 0));
		}
		else {
			if (history.size() > 1) {
				history.pop_back();
				struct history_t h = history.back();

				this->host->SetValue(h.host);
				this->port->SetValue(std::to_string(h.port));
				this->selector->SetValue(h.selector);
				data = fetcher->convert(h.host, h.port, h.selector, h.type, darkmode, nullptr);
				
			}
		}
	}

	wxString page = wxString::FromUTF8(data.c_str(), data.size());
	if (page.size() == 0) {
#ifdef _MSC_VER
		std::wstring res;
		int sz;
		sz = MultiByteToWideChar(437, 0, data.c_str(), data.size(), NULL, 0);
		if (sz > 0) {
			res.resize(sz);
			sz = MultiByteToWideChar(437, 0, data.c_str(), data.size(), &res[0], sz);
			if (sz > 0) {
				std::string str;
				sz = WideCharToMultiByte(CP_UTF8, 0, res.c_str(), -1, NULL, 0, NULL, NULL);
				str.resize(sz);
				sz = WideCharToMultiByte(CP_UTF8, 0, res.c_str(), -1, &str[0], sz, NULL, NULL);
				page = wxString::FromUTF8(str.c_str());
			}
		}
#else
		iconv_t cvt = iconv_open("UTF-8", "CP437");
		if (cvt != (iconv_t)-1) {
			std::string outd;
			char* iptr = &data[0];
			size_t inc = data.size();
			size_t ouc = data.size() * 4;
			outd.resize(ouc);
			char* optr = &outd[0];
			
			if (iconv(cvt, &iptr, &inc, &optr, &ouc) != -1) {
				page = wxString::FromUTF8(outd.c_str());
			}

			iconv_close(cvt);
		} else {
			std::cout << "Invalid Codepage!\r\n";
		}
#endif
	}

	content->SetPage(page);

}

void MyFrame::setStatus(wxString status) {
	SetStatusText(status);
}